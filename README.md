This is my Python scripts of custom Rhino commands.  
The scripts run in Rhino 6 (not confirmed in Rhino 5)

## [1.0.1] - 2019-05-24

## SlotCenter ***Added***  
[GitLab](https://gitlab.com/bathingmonkey/rhino-python/blob/master/dev/SlotCenter_cmd.py)  

## SlotCenterDist ***Changed***
[GitLab](https://gitlab.com/bathingmonkey/rhino-python/blob/master/dev/SlotCenterDistance_cmd.py)  
![alt text](/images/SlotCenterDist.gif)

## SlotCorner
[GitLab](https://gitlab.com/bathingmonkey/rhino-python/blob/master/dev/SlotCorner_cmd.py)  
![alt text](/images/SlotCorner.gif)

## ChangeObjectColor
[GitLab](https://gitlab.com/bathingmonkey/rhino-python/blob/master/dev/ChangeObjectColor_cmd.py)

## GridSwich
[GitLab](https://gitlab.com/bathingmonkey/rhino-python/blob/master/dev/GridSwitch_cmd.py)

## HideLockedObjects
[GitLab](https://gitlab.com/bathingmonkey/rhino-python/blob/master/dev/HideLockedObjects_cmd.py)

## ShowLockedObjects
[GitLab](https://gitlab.com/bathingmonkey/rhino-python/blob/master/dev/ShowLockedObjects_cmd.py)

## toggle_gumball_drag_mode
[GitLab](https://gitlab.com/bathingmonkey/rhino-python/blob/master/dev/toggle_gumball_drag_mode_cmd.py)