# Change Log
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/)
and this project adheres to [Semantic Versioning](http://semver.org/).

## [1.0.1] - 2019-05-24
### Added  
- SlotCenter_cmd.py

### Changed
- Now SlotCenter_cmd.py constrains the direction to the normal to surface.

## [1.0.0] - 2019-04-06  
### Added
- SlotCenterDist
- SlotCorner
- ChangeObjectColor
- GridSwich
- HideLockedObjects
- ShowLockedObjects
- toggle_gumball_drag_mode