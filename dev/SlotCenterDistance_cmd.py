import rhinoscriptsyntax as rs
import scriptcontext
import Rhino
import System

__commandname__ = "SlotCenterDistance"

# RunCommand is the called when the user enters the command name in Rhino.
# The command name is defined by the filname minus "_cmd.py"
def RunCommand( is_interactive ):

	print __commandname__
	
	radius_color = System.Drawing.Color.FromArgb(0, 0, 255)
	aidline_color = Rhino.ApplicationSettings.AppearanceSettings.CrosshairColor
	line_color = scriptcontext.doc.Layers.CurrentLayer.Color
	text_color =  System.Drawing.Color.FromArgb(0, 0, 0)
	onSrf_option = Rhino.Input.Custom.OptionToggle(False, "Off", "On")
	
	# Pick Centre Points
	# 1st point
	gp = Rhino.Input.Custom.GetPoint()
	gp.SetCommandPrompt("Center distance")
	gp.AcceptNumber(enable=True, acceptZero=False)
	gp.AcceptPoint(True)
	gp.AddOptionToggle("OnSrf", onSrf_option)
	while True:
	    res = gp.Get()
	    if gp.CommandResult()!=Rhino.Commands.Result.Success:
	        return gp.CommandResult()
	    if res == Rhino.Input.GetResult.Option:
	        if gp.Option().EnglishName == "OnSrf":
	            if onSrf_option.CurrentValue:
	                srf = SelSurface()
	                gp.Constrain(srf, allowPickingPointOffObject=False)
	            else:
	                gp.ClearConstraints()
	        continue
	    elif res == Rhino.Input.GetResult.Point:
	        p1 = gp.Point()
	    break
	
	# To draw on not planar surface
	if not srf.IsPlanar():
		rv, u, v = srf.ClosestPoint(p1)
		if rv:
			rv, plane = srf.FrameAt(u, v)
			if rv:
				gp.Constrain(plane, allowElevator=False)
	
	# 2nd point
	gp.ClearCommandOptions()
	gp.SetCommandPrompt("2nd point")
	gp.SetBasePoint(p1, showDistanceInStatusBar=True)
	gp.DrawLineFromPoint(p1, showDistanceInStatusBar=True)
	gp.AcceptNumber(enable=False, acceptZero=False)
	gp.ConstrainDistanceFromBasePoint(0.0)
	gp.Get()
	if( gp.CommandResult() != Rhino.Commands.Result.Success ):
	    return gp.CommandResult()
	p2 = gp.Point()
	if gp.Number() > 0:
	    v = p2 - p1
	    v.Unitize()
	    p2 = p1 + gp.Number() * v
	    
	##Check ZeroTolerance
	v = p2 - p1
	if v.IsTiny(Rhino.RhinoMath.ZeroTolerance):
	    return Rhino.Commands.Result.Nothing
	
	# dynamic draw to pick 3rd point
	def GetPointDynamicDrawFunc( sender, args ):
	    gp = args.Source  # Dynamically obtain Getpoint instance
	    #
	    centreLine = [p1, p2]
	    midPt = ( p1 + p2 ) / 2
	    p3 = args.CurrentPoint
	    cpt = rs.LineClosestPoint(centreLine, p3)
	    vec = p3 - cpt # vector from cpt to p3
	    x1 = p1 + vec # x axis for p1
	    x2 = p2 + vec # x axis for p2
	    y1 = p2 # y axis for p1
	    y2 = p1 # y axis for p2
	    plane1 = Rhino.Geometry.Plane(p1, x1, y1)
	    plane2 = Rhino.Geometry.Plane(p2, x2, y2)
	    if  (midPt - cpt).Length > (p1-p2).Length/2:
	        if (p3-p1).Length > (p3-p2).Length:
	            radius = (p3-p2).Length
	            fromPt = p2
	        else:
	            radius = (p3-p1).Length
	            fromPt = p1
	    else:
	        radius = vec.Length
	        fromPt = cpt
	        
	    angle = Rhino.RhinoMath.ToRadians(-180.0)
	    
	    if gp.Number() > 0:
	        radius = gp.Number()
	    
	    # Draw
	    gp.SetBasePoint(fromPt, showDistanceInStatusBar=True)
	    args.Display.DrawPoint(p1)
	    args.Display.DrawPoint(p2)
	    radius_line = Rhino.Geometry.Line(fromPt, p3)
	    centre_line = Rhino.Geometry.Line(p1, p2)
	    args.Display.DrawDottedLine(radius_line, aidline_color) # Radius Line 
	    args.Display.DrawDottedLine(centre_line, aidline_color) # Centre Line
	    arc1 = Rhino.Geometry.Arc(plane1, radius, angle)
	    args.Display.DrawArc(arc1, line_color)
	    arc2 = Rhino.Geometry.Arc(plane2, radius, angle)
	    args.Display.DrawArc(arc2, line_color)
	    args.Display.DrawLine(arc1.StartPoint, arc2.StartPoint, line_color)
	    args.Display.DrawLine(arc1.EndPoint, arc2.EndPoint, line_color)
	    # Draw Text
	    screen_point = Rhino.Geometry.Point2d(10, 30)
	    msg = "L={0}\nr={1}".format((p1-p2).Length, radius)
	    args.Display.Draw2dText(msg, text_color, screen_point, False)
	
	
	# Get Radius
	gp.SetCommandPrompt("Radius")
	
	gp.EnableDrawLineFromPoint(False)
	gp.DynamicDraw += GetPointDynamicDrawFunc
	gp.Get()
	if( gp.CommandResult() == Rhino.Commands.Result.Success ):
	    p3 = gp.Point()
	    radius = gp.Number()
	    drawSlot(p1, p2, p3, radius)
	
	return 0


def SelSurface():
    go = Rhino.Input.Custom.GetObject()
    go.SetCommandPrompt("Surface to place a point on")
    go.GeometryFilter = Rhino.DocObjects.ObjectType.Surface
    go.SubObjectSelect = True # to pick one face of brep
    go.DeselectAllBeforePostSelect = True
    go.OneByOnePostSelect = True
    go.Get()
    if go.CommandResult()!=Rhino.Commands.Result.Success:
        return None

    objref = go.Object(0)
    
    obj = objref.Object()
    if not obj: return None
    
    surface = objref.Surface()
    if not surface: return None
    
    obj.Select(False)
    
    return surface


def drawSlot(p1, p2, p3, r):
    # Plane from 3 ponts
    centreLine = [p1, p2]
    cpt = rs.LineClosestPoint(centreLine, p3)
    midPt= ( p1 + p2 ) / 2
    vec = p3 - cpt 
    x1 = p1 + vec # x axis for p1
    x2 = p2 + vec # x axis for p2
    y1 = p2 # y axis for p1
    y2 = p1 # y axis for p2
    plane1 = rs.PlaneFromPoints(p1 ,x1, y1)
    if plane1 == None:
        print "Can not accept colinear points" 
        return 0
    plane2 = rs.PlaneFromPoints(p2 ,x2, y2)
    if plane2 == None:
        print "Can not accept colinear points" 
        return 0

    if  (midPt - cpt).Length > (p1-p2).Length/2:
        if (p3-p1).Length > (p3-p2).Length:
            radius = (p3-p2).Length
        else:
            radius = (p3-p1).Length
    else:
        radius = vec.Length
        
    if r>0: radius = r
    
    id_arc1 = rs.AddArc(plane1, radius, -180)
    id_arc2 = rs.AddArc(plane2, radius, -180)
    
    if rs.IsCurve(id_arc1):
        ln1_spt = rs.CurveStartPoint(id_arc1)
        ln2_ept = rs.CurveEndPoint(id_arc1)
    if rs.IsCurve(id_arc2):
        ln1_ept = rs.CurveStartPoint(id_arc2)
        ln2_spt = rs.CurveEndPoint(id_arc2)
    
    id_ln1 = rs.AddLine(ln1_spt, ln1_ept)
    id_ln2 = rs.AddLine(ln2_spt, ln2_ept)
    
    objs = [id_arc1, id_arc2, id_ln1, id_ln2]
    if objs: rs.JoinCurves(objs, delete_input=True, tolerance=None)


if( __name__ == "__main__" ):
    RunCommand(True)
