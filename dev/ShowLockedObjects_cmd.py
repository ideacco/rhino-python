# 2017/7/28

import rhinoscriptsyntax as rs
import scriptcontext
from Rhino import *
from scriptcontext import doc
import rhinoscriptsyntax as rs

__commandname__ = "ShowLockedObjects"

# RunCommand is the called when the user enters the command name in Rhino.
# The command name is defined by the filname minus "_cmd.py"
def RunCommand( is_interactive ):
    #print __commandname__
    if scriptcontext.sticky.has_key("LockedObjects"):
        m_ids = scriptcontext.sticky["LockedObjects"]
    else:
        print "No record for any locked object"
        return Commands.Result.Failure

    scriptcontext.sticky["LockedObjects"] = []

    print m_ids


    if not m_ids:
        print "No objects"
        return Commands.Result.Failure

    rs.ShowObjects(m_ids)
    rs.LockObjects(m_ids)





    # you can optionally return a value from this function
    # to signify command result. Return values that make
    # sense are
    # 0 == success
    # 1 == cancel
    # If this function does not return a value, success is assumed
    return Commands.Result.Success




#This allows you to test the script from the editor, debug etc.
#RunCommand(True)

if( __name__ == "__main__" ):
    RunCommand(True)
