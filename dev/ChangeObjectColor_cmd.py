# 2018/3/31
import rhinoscript.userinterface
import rhinoscript.geometry
import rhinoscriptsyntax as rs

__commandname__ = "ChangeObjectColor"

# RunCommand is the called when the user enters the command name in Rhino.
# The command name is defined by the filname minus "_cmd.py"
def RunCommand( is_interactive ):
    objs = rs.GetObjects("Select objects to change color")
    if objs:
        color = rhinoscript.userinterface.GetColor(0)
    if color:
        for obj in objs: rs.ObjectColor( obj, color )
  
    # you can optionally return a value from this function
    # to signify command result. Return values that make
    # sense are
    #   0 == success
    #   1 == cancel
    # If this function does not return a value, success is assumed
    return 0
if( __name__ == "__main__" ):
    RunCommand(True)
