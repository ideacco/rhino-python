import rhinoscriptsyntax as rs
from Rhino import *

__commandname__ = "toggle_gumball_drag_mode"

# RunCommand is the called when the user enters the command name in Rhino.
# The command name is defined by the filname minus "_cmd.py"
def RunCommand( is_interactive ):
  #print "Tggle drag mode", __commandname__
  current_mode = ApplicationSettings.ModelAidSettings.SnappyGumballEnabled

  ApplicationSettings.ModelAidSettings.SnappyGumballEnabled = not current_mode
  if current_mode:
      print("Smooth Dragging Mode")
  else:
      print("Snappy Dragging Mode")
  # you can optionally return a value from this function
  # to signify command result. Return values that make
  # sense are
  #   0 == success
  #   1 == cancel
  # If this function does not return a value, success is assumed
  return 0

#This allows you to test the script from the editor, debug etc.
#RunCommand(True)
if( __name__ == "__main__" ):
    RunCommand(True)
